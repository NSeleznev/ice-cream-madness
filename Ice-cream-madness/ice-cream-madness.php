<?php
/*
Plugin Name: Ice-cream-madness
Plugin URI: https://vk.com/n.seleznev
Description: Just ice cream calculater for 2UP
Version: 2.0
Author: N.N.Seleznev
Author URI: https://vk.com/n.seleznev
*/

/*  
Copyright 2016  Nikolay Seleznev  (email: nnseleznev@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

global $icm_db_version;
$icm_db_version = '2.0';

/* *УСТАОВКА, ДЕАКТИВАЦИЯ, УДАЛЕНИЕ ПЛАГИНА* */

// Создаем опцию и таблицу при установке плагина.
function icm_db_activation() {
	global $wpdb;
	global $icm_db_version;

	$table_name = $wpdb->prefix . 'icecream_madness';
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
		id int(10) NOT NULL AUTO_INCREMENT,
		type VARCHAR(250) NOT NULL,
		name VARCHAR(250) NOT NULL,
		price VARCHAR(250) NOT NULL ,
		description VARCHAR(250) NOT NULL,
		PRIMARY KEY (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	$wpdb->query($sql);

	add_option( 'icm_db_version', $icm_db_version );
}

// Удаляем опцию при деактивации плагина.
function icm_db_deactivation() {
	delete_option( 'icm_db_version' );
}

// Удаляем таблицу при деинсталяции плагина.
function icm_db_uninstall()
{
	global $wpdb;
	$table_name = $wpdb->prefix . 'icecream_madness';
	$sql = "DROP TABLE $table_name";

	$wpdb->query( $sql );
}
register_activation_hook( __FILE__, 'icm_db_activation' );
register_deactivation_hook( __FILE__, 'icm_db_deactivation');
register_uninstall_hook( __FILE__, 'icm_db_uninstall');
?>