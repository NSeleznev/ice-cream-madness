<?php
// Регистрируем пункт меню в панели администратора.
function register_icm_admin_menu() {
$page_title = 'Icecream Madness';
$menu_title = 'Icecream<br>Madness';
$capability = 'manage_options';
$menu_slug = 'icecream-madness';
$function = 'icm_admin_menu';
$icon_url = 'dashicons-smiley';
$position = 2;

add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
}
// Код страницы плгина
function icm_admin_menu() {
	echo '<h1>Icecream Madness</h1>';
	echo '<h2>Добавить компонент</h2>';
	icm_add_component();
	echo '<h2>Список компонентов</h2>';
	icm_edit_component();
}
add_action( 'admin_menu', 'register_icm_admin_menu' );
?>