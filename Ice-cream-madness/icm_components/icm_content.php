<?php
// Вывод формы калькулятора мороженного

function icm_shortcode() {
	
	global $wpdb;
	$table_name = $wpdb->prefix . 'icecream_madness';
	$icm_component = $wpdb -> get_results( "SELECT * FROM $table_name" );
	
	ob_start();
	
	print_r($icm_component);

	return ob_get_clean();
}
add_shortcode('icecream-madness', 'icm_shortcode');
?>