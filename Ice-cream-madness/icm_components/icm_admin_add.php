<?php
// Создание и вывод формы добавления компонента
function icm_add_component() {
	global $wpdb;
	$table_name = $wpdb->prefix . 'icecream_madness';
	
	// Сохранение компонента в таблицу
	if ( isset( $_POST['icm_add_component_btn'] ) ) {
		
		$icm_component_type = $_POST['icm_component_type'];
		$icm_component_name = $_POST['icm_component_name'];
		$icm_component_price = $_POST['icm_component_price'];
		$icm_component_description = $_POST['icm_component_description'];
		
		if ( $icm_component_type !== '' && $icm_component_name !== '' && $icm_component_price !== '' && $icm_component_description !== '' ) {
			
			$wpdb->insert(
				$table_name,
				array(
				'type' => $icm_component_type,
				'name' => $icm_component_name,
				'price' => $icm_component_price,
				'description' => $icm_component_description),
				array('%s', '%s', '%s', '%s')
			);
			
		} else {
			
			echo 'Заполните все поля';
			
		}
	}
	// Форма добавления компонента
	echo
		'
			<form class="icm_add_component" name="icm_add_component" method="post" action="'.$_SERVER['PHP_SELF'].'?page=icecream-madness" style="margin-bottom: 80px;">
				<p>Компонент</p>
				<input type="text" name="icm_component_type">
				<p>Наименование</p>
				<input type="text" name="icm_component_name">
				<p>Цена</p>
				<input type="text" name="icm_component_price">
				<p>Описание</p>
				<textarea name="icm_component_description" maxlength="850"></textarea>
				<div>
					<input type="submit" name="icm_add_component_btn" value ="Добавить">
				</div>
			</form>
		';
}
?>