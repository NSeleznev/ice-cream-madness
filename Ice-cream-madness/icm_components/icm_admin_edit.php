<?php
// Создание и вывод формы редактирования компонента
function icm_edit_component() {
	global $wpdb;
	$table_name = $wpdb->prefix . 'icecream_madness';
	
	// Редактирование компонента в таблице
	if ( isset( $_POST['icm_edit_component_btn'] ) ) {
		
		$icm_component_id = $_POST['icm_component_id'];
		$icm_component_type = $_POST['icm_component_type'];
		$icm_component_name = $_POST['icm_component_name'];
		$icm_component_price = $_POST['icm_component_price'];
		$icm_component_description = $_POST['icm_component_description'];
		
		if ( $icm_component_type !== '' && $icm_component_name !== '' && $icm_component_price !== '' && $icm_component_description !== '' ) {
			
			$wpdb->update(
				$table_name,
				array(
				'type' => $icm_component_type,
				'name' => $icm_component_name,
				'price' => $icm_component_price,
				'description' => $icm_component_description),
				array( 'id' => $icm_component_id ),
				array('%s', '%s', '%s', '%s'),
				array( '%d' )
			);
			
		} else {
			
			echo 'Заполните все поля';
			
		}
	}
	
	// Удаление компонента из таблицы
	if ( isset( $_POST['icm_delete_component_btn'] ) ) {
		
		$icm_component_id = $_POST['icm_component_id'];
		
		$wpdb->delete( $table_name, array( 'id' => $icm_component_id ), array( '%d' ) );
		
	}
	
	// Форма редактирования компонента
	$icm_component = $wpdb -> get_results( "SELECT * FROM $table_name" );
	
	if ( $icm_component == null ) {
		
		echo 'Компоненты не найдены';
		
	} else {
	
		foreach ( $icm_component as $item ) {
			$icm_id = $item->id;
			$icm_type = $item->type;
			$icm_name = $item->name;
			$icm_price = $item->price;
			$icm_description = $item->description;
			
			echo
				'
				<h3>'.$icm_name.'&nbsp;'.$icm_type.'</h3>
				<form class="icm_edit_component" name="icm_edit_component" method="post" action="'.$_SERVER['PHP_SELF'].'?page=icecream-madness">
					<input type="hidden" name="icm_component_id" value="'.$icm_id.'">
					<p>Компонент</p>
					<input type="text" name="icm_component_type" value="'.$icm_type.'">
					<p>Наименование</p>
					<input type="text" name="icm_component_name" value="'.$icm_name.'">
					<p>Цена</p>
					<input type="text" name="icm_component_price" value="'.$icm_price.'">
					<p>Описание</p>
					<textarea name="icm_component_description" maxlength="850">'.$icm_description.'</textarea>
					<div>
						<input type="submit" name="icm_edit_component_btn" value="Сохранить">
						<input type="submit" name="icm_delete_component_btn" value="Удалить">
					</div>
				</form>
				';
		}
	}
}
?>